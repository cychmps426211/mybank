import React, { useState } from "react";
import { Button, Form, FormGroup, Input } from "reactstrap";
import axios from 'axios';
import { useAppContext } from "./libs/contextLib";
import './Login.css'

export default function Login(props) {
    const { userHasAuthenticated } = useAppContext();
    const [userid, setUserid] = useState("");

    function validateForm() {
        return userid.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();

        var apiurl = "http://localhost:3333/account/" + userid;
        axios.get(apiurl)
            .then(function (res) {
                if (res.status === 200) {
                    userHasAuthenticated(true);
                }
            })
            .catch((error) => { alert(error) });
    }

    return (
        <div className="Login">
            <Form onSubmit={handleSubmit}>
                <FormGroup controlId="userid" bsSize="large">
                    <Input
                        autoFocus
                        type="text"
                        value={userid}
                        placeholder="User ID"
                        onChange={e => setUserid(e.target.value)} 
                    />
                </FormGroup>
                <Button
                    block
                    type="submit"
                    bsSize="large"
                    color="primary"
                    disabled={!validateForm()}
                >
                    Login
                </Button>
            </Form>
        </div>
    );
}
