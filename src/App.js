import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from "./libs/contextLib";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import {
    Collapse,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavLink
} from 'reactstrap';
import Routes from './Routes'



function App() {
    const [isAuthenticated, userHasAuthenticated] = useState(false);

    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    function handleLogout() {
        userHasAuthenticated(false);
    }
    
    return (
        <div className="App">
            <Navbar color="dark" dark expand="md">
                <NavbarBrand className="LinkNav" href="/">MyBank</NavbarBrand>
                
                {isAuthenticated
                    ? <NavbarToggler onClick={toggle}></NavbarToggler>
                    : <></>
                }
                {isAuthenticated
                    ? 
                      <Collapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar></Nav>
                        <NavLink className="LogoutLink" onClick={handleLogout}>Logout</NavLink>
                      </Collapse>
                    : <></>
                }
            </Navbar>

			<AppContext.Provider value={{ isAuthenticated, userHasAuthenticated }}>
                <Routes />
            </AppContext.Provider>
        </div>
    );
}

export default App;