import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "./Login"
import NotFound from "./NotFound"

export default function Routes() {
    return (
        <Switch>
            <Route path="/" exact component={Login} />

            { /* Finally, catch all unmatched routes */ }
            <Route component={NotFound} />
        </Switch>
    );
}