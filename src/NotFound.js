import React from 'react';
import './NotFound.css';

export default function NotFound() {
    return (
        <div className="NotFound">
            <h3>404 NOT found</h3>
        </div>
    );
}